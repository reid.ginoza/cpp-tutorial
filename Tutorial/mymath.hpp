//
//  mymath.hpp
//  Tutorial
//
//  Created by Reid Ginoza on 12/17/21.
//  Copyright © 2021 Reid Ginoza. All rights reserved.
//

#ifndef mymath_hpp
#define mymath_hpp

#include <iostream>

int sum(int num1, int num2);

std::string sumMessage(int num1, int num2);

#endif /* mymath_hpp */
