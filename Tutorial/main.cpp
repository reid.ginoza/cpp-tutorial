//
//  main.cpp
//  Tutorial
//
//  Created by Reid Ginoza on 12/16/21.
//  Copyright © 2021 Reid Ginoza. All rights reserved.
//

#include <iostream>
using namespace std;

#include "mymath.hpp"

/**
 * This next section handles all of the different
 * choices available in this program.
 */
// admittedly, it does feel like a lot of boilerplate for not a lot of benefit.
const string SUM = "sum";
const string ASCII_LOOKUP = "ascii";
const string LETTER_SEQUENCE = "letter_seq";

enum Command {sum_int, ascii_lookup, letter_sequence, not_found};

Command resolveChoice(string cmd) {
    if (cmd == SUM) {
        return sum_int;
    } else if (cmd == ASCII_LOOKUP) {
        return ascii_lookup;
    } else if (cmd == LETTER_SEQUENCE) {
        return letter_sequence;
    } else {
        return not_found;
    }
}


const string CHOOSE_CMD = R"(Please type in [)" + SUM + R"(|)" + ASCII_LOOKUP + R"(|)" + LETTER_SEQUENCE + R"(] to select your program.)";


int run_sum() {
    int num1;
    int num2;
    
    cout << "Please enter two integers separated by a space." << endl;
    cin >> num1 >> num2;
    
    cout << sumMessage(num1, num2) << endl;
    cin.get();
    
    return 0;
}

int run_ascii_lookup() {
    char char_in;
    cout << "Please enter a single character." << endl;
    cin >> char_in;
    
    cout << "ASCII value is " << (int)char_in << endl;
    return 0;
}

int run_cmd_not_found(string choice){
    cout << "Program " << choice << " not found. Exiting." << endl;
    return 1;
}

/**
 * Takes a starting letter and the number of
 * letters to continue to in the ASCII sequence.
 * Assumes input is entered in correct type.
 * Does not continue if the char is not in the
 * ASCII table or if the sequence goes above
 * the end of the ASCII table.
 */
int run_letter_sequence(){
    u_int length;
    char startChar;
    int curDec;
    
    cout << "Please enter a starting character and number of characters to include in the sequence." << endl;
    
    cin >> startChar >> length;
    const int startDec = (int) startChar;
    
    if (startDec < 0 || startDec > 127) {
        cout << "Character " << startChar << " not found in ASCII table." << endl;
        return 0;
    }
    
    for (int i = 0; i < length; i++) {
        curDec = startDec + i;
        
        if (curDec > 127) {
            cout << "Reached the end of the ASCII table." << endl;
            
            break;
        }
        
        cout << curDec << " " << (char) curDec << endl;
    }
    
    return 0;
}


int main(int argc, const char * argv[]) {
    string choice;
    
    cout << "Hello, World!\n" << "Press Enter." << endl;
    cin.get();
    
    cout << CHOOSE_CMD << endl;
    cin >> choice;
    
    switch (resolveChoice(choice)) {
        case sum_int:
            return run_sum();
        case ascii_lookup:
            return run_ascii_lookup();
        case letter_sequence:
            return run_letter_sequence();
        default:
            return run_cmd_not_found(choice);
    }
    
    cout << "Should not end up here." << endl;
    return 1;
}
