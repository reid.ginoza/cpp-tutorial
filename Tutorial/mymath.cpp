//
//  mymath.cpp
//  Tutorial
//
//  Created by Reid Ginoza on 12/17/21.
//  Copyright © 2021 Reid Ginoza. All rights reserved.
//

#include "mymath.hpp"

int sum(int num1, int num2) {
    return num1 + num2;
}

std::string sumMessage(int num1, int num2) {
    int s;
    std::string str;
    
    s = sum(num1, num2);
    
    str.append(std::to_string(num1));
    str.append(" + ");
    str.append(std::to_string(num2));
    str.append(" = ");
    str.append(std::to_string(s));
    return str;
}
